package sample

import (
	"testing"

	"gitee.com/woodpile/utils.go/log"
)

func TestRunSample(t *testing.T) {
	RunSample(1)

	log.SetGlobalHandlerConfig(&log.HandlerConfig{
		Level:            log.GlobalLevel(),
		Seperator:        "|",
		FmtSeperator:     "=",
		BCodeLine:        true,
		BUseJSONAssemble: true,
		ContextPoolSize:  4,
	})
	log.SetGlobalHandler(log.NewHandler())
	log.BCodeLineDetail = true

	RunSample(2)
}

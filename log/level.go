package log

//define log level
const (
	LvlInvalid = int(iota) //0
	LvlTrace               //1
	LvlDebug               //2
	LvlInfo                //3
	LvlWarning             //4
	LvlError               //5
	LvlFatal               //6
)

var allLevels = []int{LvlTrace, LvlDebug, LvlInfo, LvlWarning, LvlError, LvlFatal}

//whether need ignore by level checking.
//d is defined level switch. c is current level that be checked.
func levelIgnore(d, c int) bool {
	return c < d
}

//whether need ignore by level checking with globel level switch.
//d is defined level switch. c is current level that be checked.
func levelIgnoreGlobal(c int) bool {
	return levelIgnore(GlobalLevel(), c)
}

//level name string
var lvlName = []string{
	"",
	"TRC",
	"DBG",
	"INF",
	"WAR",
	"ERR",
	"FAT",
}

func getlevelname(l int) string {
	return lvlName[l]
}

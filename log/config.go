package log

import (
	"runtime"
	"strings"
)

//HandlerConfig define config options of Handler
type HandlerConfig struct {
	//log level
	Level int `json:"level,omitempty"`

	//seperator char between message field
	Seperator string `json:"seperator,omitempty"`
	//seperator char in format message field
	FmtSeperator string `json:"fmt_seperator,omitempty"`

	//switch of code line outputting
	BCodeLine bool `json:"b_code_line,omitempty"`

	//switch of assemble message using JSON
	BUseJSONAssemble bool `json:"b_use_json_assemble,omitempty"`

	//size of idle context instance in handler context pool
	ContextPoolSize int
}

//global handler config
var globalHandlerConfig = &HandlerConfig{
	Level:            LvlDebug,
	Seperator:        "|",
	FmtSeperator:     "=",
	BCodeLine:        false,
	BUseJSONAssemble: false,
	ContextPoolSize:  4,
}

//GlobalHandlerConfig get global handler config
func GlobalHandlerConfig() *HandlerConfig {
	return globalHandlerConfig
}

//SetGlobalHandlerConfig set global handler config
func SetGlobalHandlerConfig(cfg *HandlerConfig) {
	globalHandlerConfig = cfg
}

//GlobalLevel get current global level.
//Global level is also the switch of log level check.
func GlobalLevel() int {
	return globalHandlerConfig.Level
}

//SetGlobalLevel set current global level. return the new level after set.
//Global level is also the switch of log level check.
func SetGlobalLevel(l int) int {
	globalHandlerConfig.Level = l
	globalhandler.SetLevel(GlobalLevel())
	return GlobalLevel()
}

var (
	//codeLineTrimPrefix if it is not "", code line info in log message will be whole path without trimmed prefix.
	codeLineTrimPrefix = ""

	//BCodeLineDetail switch of detail code line string
	BCodeLineDetail = false
)

func init() {
	_, fn, _, ok := runtime.Caller(0)
	if ok {
		codeLineTrimPrefix = strings.TrimSuffix(fn, "utils.go/log/config.go")
	}
}

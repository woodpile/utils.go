package log

import (
	"reflect"
	"testing"
)

var (
	utHandlerCommon01 = &HandlerCommon{
		Sep:             "|",
		SepFmt:          "=",
		TimeFmt:         nil,
		Targets:         append(make([]Target, 0, 4), defaultTargets...),
		Prefixes:        make([][2]string, 0, 4),
		PrefixStr:       "",
		BCodeLine:       false,
		UseJSONAssemble: false,
	}
)

func TestNewContextPool(t *testing.T) {
	type args struct {
		common *HandlerCommon
		size   int
	}
	tests := []struct {
		name string
		args args
		want *ContextPool
	}{
		{
			name: "ok.0",
			args: args{
				common: utHandlerCommon01,
				size:   4,
			},
			want: &ContextPool{
				common: utHandlerCommon01,
				pool:   nil,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewContextPool(tt.args.common, tt.args.size)
			got.pool = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewContextPool() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextPool_new(t *testing.T) {
	type args struct {
		level int
	}
	tests := []struct {
		name string
		p    *ContextPool
		args args
		want ContextAllowTar
	}{
		{
			name: "ok.0",
			p:    NewContextPool(utHandlerCommon01, 4),
			args: args{level: LvlInfo},
			want: _utGetEmptyContextBase(utHandlerCommon01, LvlInfo),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.p.new(tt.args.level)
			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextPool.new() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextPool_Get(t *testing.T) {
	type args struct {
		level int
	}
	tests := []struct {
		name string
		p    *ContextPool
		args args
		want ContextAllowTar
	}{
		{
			name: "ok.0",
			p:    NewContextPool(utHandlerCommon01, 4),
			args: args{level: LvlInfo},
			want: _utGetEmptyContextBase(utHandlerCommon01, LvlInfo),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.p.Get(tt.args.level)
			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextPool.Get() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextPool_release(t *testing.T) {
	type args struct {
		ctx Context
	}
	tests := []struct {
		name string
		p    *ContextPool
		args args
	}{
		{
			name: "ok.0",
			p:    NewContextPool(utHandlerCommon01, 4),
			args: args{
				ctx: NewContextBase(utHandlerCommon01, LvlInfo),
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.p.release(tt.args.ctx)
		})
	}
}

func TestContextPool_MoreOverSize(t *testing.T) {
	pool := NewContextPool(utHandlerCommon01, 4)
	count := 100
	tmp := make([]Context, 0, count)
	for i := 0; i < count; i++ {
		ctx := pool.Get(LvlDebug)
		tmp = append(tmp, ctx)
	}
	for _, ctx := range tmp {
		ctx.(*ContextBase).release()
	}
}

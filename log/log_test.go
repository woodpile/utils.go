package log

import (
	"strings"
	"testing"
	"time"
)

func TestHandlerOutput(t *testing.T) {
	h, fake := makeHandlerWithFakeTargetOnly(nil)
	h.SetLevel(LvlDebug)
	h.BindTimeFmt(func(*strings.Builder, time.Time) {})
	h.AddPrefix("1", "pipimi")

	h.Ctx().P("lalala").
		PF("tt%dll%.2f", 83, 5.6).
		P("biabiabia").
		Z()

	expectCount := 1
	expectLogMessage := []string{
		"|DBG|pipimi|lalala|tt83ll5.60|biabiabia",
	}
	if got := fake.Count(); expectCount != got {
		t.Errorf("Expect fake.Count() is %v, actual is %v", expectCount, got)
	}
	for _, expectMsg := range expectLogMessage {
		if got := fake.Pop(); expectMsg != got {
			t.Errorf("Expect fake.Pop() is %v, actual is %v", expectMsg, got)
		}
	}
	if got := fake.Count(); got != 0 {
		t.Errorf("Expect fake.Count() is %v, actual is %v", 0, got)
	}

	h.Ctx().Pre("popoco").
		P("biabiabia").
		Z()

	expectCount = 1
	expectLogMessage = []string{
		"|DBG|pipimi|popoco|biabiabia",
	}
	if got := fake.Count(); expectCount != got {
		t.Errorf("Expect fake.Count() is %v, actual is %v", expectCount, got)
	}
	for _, expectMsg := range expectLogMessage {
		if got := fake.Pop(); expectMsg != got {
			t.Errorf("Expect fake.Pop() is %v, actual is %v", expectMsg, got)
		}
	}
	if got := fake.Count(); got != 0 {
		t.Errorf("Expect fake.Count() is %v, actual is %v", 0, got)
	}
}

func TestStaticOutput(t *testing.T) {
	fake := &FakeTarget{}
	// globalhandler.BindTimeFmt(func(t time.Time) string { return "" })
	globalhandler.DelTarget("stdout")
	globalhandler.AddTarget(fake)
	SetGlobalLevel(LvlInfo)

	Log().Ctx().P("static").PF("mmnn").P("zzz...").Z()

	expectCount := 1
	expectLogMessage := []string{
		"INF|static|mmnn|zzz...",
	}
	if got := fake.Count(); expectCount != got {
		t.Errorf("Expect fake.Count() is %v, actual is %v", expectCount, got)
	}
	for _, expectMsg := range expectLogMessage {
		got := fake.Pop()
		gots := strings.SplitN(got, "|", 2)
		if len(gots) != 2 {
			t.Errorf("Expect fake.Pop() is %v, not a format log message", got)
			continue
		}
		gottime := gots[0]
		if !strings.HasPrefix(gottime, time.Now().Format("2006-01-02 15:04")) {
			t.Errorf("Expect fake.Pop() time string format is %v, actual is %v",
				"2006-01-02 15:04", gottime)
		}
		gotmsg := gots[1]
		if expectMsg != gotmsg {
			t.Errorf("Expect fake.Pop() message is %v, actual is %v", expectMsg, gotmsg)
		}
	}
	if got := fake.Count(); got != 0 {
		t.Errorf("Expect fake.Count() is %v, actual is %v", 0, got)
	}

	WAR().P("static").PF("mmnn").P("zzz...").Z()

	expectCount = 1
	expectLogMessage = []string{
		"WAR|static|mmnn|zzz...",
	}
	if got := fake.Count(); expectCount != got {
		t.Errorf("Expect fake.Count() is %v, actual is %v", expectCount, got)
	}
	for _, expectMsg := range expectLogMessage {
		got := fake.Pop()
		gots := strings.SplitN(got, "|", 2)
		if len(gots) != 2 {
			t.Errorf("Expect fake.Pop() is %v, not a format log message", got)
			continue
		}
		gottime := gots[0]
		if !strings.HasPrefix(gottime, time.Now().Format("2006-01-02 15:04")) {
			t.Errorf("Expect fake.Pop() time string format is %v, actual is %v",
				"2006-01-02 15:04", gottime)
		}
		gotmsg := gots[1]
		if expectMsg != gotmsg {
			t.Errorf("Expect fake.Pop() message is %v, actual is %v", expectMsg, gotmsg)
		}
	}
	if got := fake.Count(); got != 0 {
		t.Errorf("Expect fake.Count() is %v, actual is %v", 0, got)
	}
}

func TestOutputTimeString(t *testing.T) {
	fake := &FakeTarget{}
	globalhandler.DelTarget("stdout")
	globalhandler.AddTarget(fake)
	SetGlobalLevel(LvlDebug)

	Log().Ctx().P("timestring").Z()

	expectCount := 1
	expectLogMessage := []string{
		"DBG|timestring",
	}
	if got := fake.Count(); expectCount != got {
		t.Errorf("Expect fake.Count() is %v, actual is %v", expectCount, got)
	}
	for _, expectMsg := range expectLogMessage {
		got := fake.Pop()
		gots := strings.SplitN(got, "|", 2)
		if len(gots) != 2 {
			t.Errorf("Expect fake.Pop() is %v, not a format log message", got)
		}
		gottime := gots[0]
		if !strings.HasPrefix(gottime, time.Now().Format("2006-01-02 15:04")) {
			t.Errorf("Expect fake.Pop() time string format is %v, actual is %v",
				"2006-01-02 15:04", gottime)
		}
		gotmsg := gots[1]
		if expectMsg != gotmsg {
			t.Errorf("Expect fake.Pop() message is %v, actual is %v", expectMsg, gotmsg)
		}
	}
	if got := fake.Count(); got != 0 {
		t.Errorf("Expect fake.Count() is %v, actual is %v", 0, got)
	}

	globalhandler.BindTimeFmt(BuiltinTimeFmtCompact)

	Log().Ctx().P("timestring").Z()

	expectCount = 1
	expectLogMessage = []string{
		"DBG|timestring",
	}
	if got := fake.Count(); expectCount != got {
		t.Errorf("Expect fake.Count() is %v, actual is %v", expectCount, got)
	}
	for _, expectMsg := range expectLogMessage {
		got := fake.Pop()
		gots := strings.SplitN(got, "|", 2)
		if len(gots) != 2 {
			t.Errorf("Expect fake.Pop() is %v, not a format log message", got)
		}
		gottime := gots[0]
		if !strings.HasPrefix(gottime, time.Now().Format("200601021504")) {
			t.Errorf("Expect fake.Pop() time string format is %v, actual is %v",
				"200601021504", gottime)
		}
		gotmsg := gots[1]
		if expectMsg != gotmsg {
			t.Errorf("Expect fake.Pop() message is %v, actual is %v", expectMsg, gotmsg)
		}
	}
	if got := fake.Count(); got != 0 {
		t.Errorf("Expect fake.Count() is %v, actual is %v", 0, got)
	}
}

func TestHandlerFullOutput(t *testing.T) {
	h, fake := makeHandlerWithFakeTargetOnly(nil)
	h.SetLevel(LvlInfo)
	h.BindTimeFmt(func(*strings.Builder, time.Time) {})
	h.AddPrefix("1", "pipimi")

	h.WAR().Pre("popoco").
		FS("env", "no").
		FU("uid", 9001).
		FX("mask", 0x42).
		P("lalala").
		PF("tt%dll%.2f", 83, 5.6).
		P("biabiabia").
		Z()

	expectCount := 1
	expectLogMessage := []string{
		"|WAR|pipimi|popoco|env=no|uid=9001|mask=0x42|lalala|tt83ll5.60|biabiabia",
	}
	if got := fake.Count(); expectCount != got {
		t.Errorf("Expect fake.Count() is %v, actual is %v", expectCount, got)
	}
	for _, expectMsg := range expectLogMessage {
		if got := fake.Pop(); expectMsg != got {
			t.Errorf("Expect fake.Pop() is %v, actual is %v", expectMsg, got)
		}
	}
	if got := fake.Count(); got != 0 {
		t.Errorf("Expect fake.Count() is %v, actual is %v", 0, got)
	}
}

func TestHandlerMultiTarget(t *testing.T) {
	h, fake01 := makeHandlerWithFakeTargetOnly(nil)
	h.SetLevel(LvlInfo)
	h.BindTimeFmt(func(*strings.Builder, time.Time) {})
	h.AddPrefix("1", "pipimi")

	{
		h.WAR().Pre("popoco").
			FS("env", "no").
			FU("uid", 9001).
			FX("mask", 0x42).
			P("lalala").
			PF("tt%dll%.2f", 83, 5.6).
			P("biabiabia").
			Z()

		expectCount := 1
		expectLogMessage := []string{
			"|WAR|pipimi|popoco|env=no|uid=9001|mask=0x42|lalala|tt83ll5.60|biabiabia",
		}
		if got := fake01.Count(); expectCount != got {
			t.Errorf("Expect fake01.Count() is %v, actual is %v", expectCount, got)
		}
		for _, expectMsg := range expectLogMessage {
			if got := fake01.Pop(); expectMsg != got {
				t.Errorf("Expect fake01.Pop() is %v, actual is %v", expectMsg, got)
			}
		}
		if got := fake01.Count(); got != 0 {
			t.Errorf("Expect fake01.Count() is %v, actual is %v", 0, got)
		}
	}

	fake02 := &FakeTarget{}
	{
		h.DBG().Tar(fake02).
			P("lao chen").
			Z()

		expectCount := 1
		expectLogMessage := []string{
			"|DBG|pipimi|lao chen",
		}
		if got := fake01.Count(); expectCount != got {
			t.Errorf("Expect fake01.Count() is %v, actual is %v", expectCount, got)
		}
		if got := fake02.Count(); expectCount != got {
			t.Errorf("Expect fake02.Count() is %v, actual is %v", expectCount, got)
		}
		for _, expectMsg := range expectLogMessage {
			if got := fake01.Pop(); expectMsg != got {
				t.Errorf("Expect fake01.Pop() is %v, actual is %v", expectMsg, got)
			}
			if got := fake02.Pop(); expectMsg != got {
				t.Errorf("Expect fake02.Pop() is %v, actual is %v", expectMsg, got)
			}
		}
		if got := fake01.Count(); got != 0 {
			t.Errorf("Expect fake01.Count() is %v, actual is %v", 0, got)
		}
		if got := fake02.Count(); got != 0 {
			t.Errorf("Expect fake02.Count() is %v, actual is %v", 0, got)
		}
	}

	{
		h.DBG().
			P("xing xiong").
			Z()

		expectCount := 1
		expectLogMessage := []string{
			"|DBG|pipimi|xing xiong",
		}
		if got := fake01.Count(); expectCount != got {
			t.Errorf("Expect fake01.Count() is %v, actual is %v", expectCount, got)
		}
		if got := fake02.Count(); got != 0 {
			t.Errorf("Expect fake02.Count() is %v, actual is %v", 0, got)
		}
		for _, expectMsg := range expectLogMessage {
			if got := fake01.Pop(); expectMsg != got {
				t.Errorf("Expect fake01.Pop() is %v, actual is %v", expectMsg, got)
			}
			if got := fake02.Pop(); got != "" {
				t.Errorf("Expect fake02.Pop() is %v, actual is %v", "", got)
			}
		}
		if got := fake01.Count(); got != 0 {
			t.Errorf("Expect fake01.Count() is %v, actual is %v", 0, got)
		}
	}

	fake03 := &FakeTarget{}
	{
		h.AddTarget(fake02)

		h.DBG().UniTar(fake03).
			P("gu mi").
			Z()

		expectCount := 1
		expectLogMessage := []string{
			"|DBG|pipimi|gu mi",
		}
		if got := fake01.Count(); got != 0 {
			t.Errorf("Expect fake01.Count() is %v, actual is %v", 0, got)
		}
		if got := fake02.Count(); got != 0 {
			t.Errorf("Expect fake02.Count() is %v, actual is %v", 0, got)
		}
		if got := fake03.Count(); expectCount != got {
			t.Errorf("Expect fake03.Count() is %v, actual is %v", expectCount, got)
		}
		for _, expectMsg := range expectLogMessage {
			if got := fake01.Pop(); got != "" {
				t.Errorf("Expect fake01.Pop() is %v, actual is %v", "", got)
			}
			if got := fake02.Pop(); got != "" {
				t.Errorf("Expect fake02.Pop() is %v, actual is %v", "", got)
			}
			if got := fake03.Pop(); expectMsg != got {
				t.Errorf("Expect fake03.Pop() is %v, actual is %v", expectMsg, got)
			}
		}
		if got := fake03.Count(); got != 0 {
			t.Errorf("Expect fake03.Count() is %v, actual is %v", 0, got)
		}
	}
}

func TestHandlerSetDefaultTarget(t *testing.T) {
	fake01 := &FakeTarget{}
	SetDefaultTargets([]Target{fake01})

	{
		h := NewHandler()
		h.BindTimeFmt(func(*strings.Builder, time.Time) {})
		h.SetLevel(LvlInfo)
		h.WAR().Pre("popoco").
			FS("env", "no").
			FU("uid", 9001).
			FX("mask", 0x42).
			P("lalala").
			PF("tt%dll%.2f", 83, 5.6).
			P("biabiabia").
			Z()

		expectCount := 1
		expectLogMessage := []string{
			"|WAR|popoco|env=no|uid=9001|mask=0x42|lalala|tt83ll5.60|biabiabia",
		}
		if got := fake01.Count(); expectCount != got {
			t.Errorf("Expect fake01.Count() is %v, actual is %v", expectCount, got)
		}
		for _, expectMsg := range expectLogMessage {
			if got := fake01.Pop(); expectMsg != got {
				t.Errorf("Expect fake01.Pop() is %v, actual is %v", expectMsg, got)
			}
		}
		if got := fake01.Count(); got != 0 {
			t.Errorf("Expect fake01.Count() is %v, actual is %v", 0, got)
		}
	}

	fake02 := &FakeTarget{}
	{
		SetDefaultTargets([]Target{fake01, fake02})
		h := NewHandler()
		h.BindTimeFmt(func(*strings.Builder, time.Time) {})
		h.DBG().
			P("lao chen").
			Z()

		expectCount := 1
		expectLogMessage := []string{
			"|DBG|lao chen",
		}
		if got := fake01.Count(); expectCount != got {
			t.Errorf("Expect fake01.Count() is %v, actual is %v", expectCount, got)
		}
		if got := fake02.Count(); expectCount != got {
			t.Errorf("Expect fake02.Count() is %v, actual is %v", expectCount, got)
		}
		for _, expectMsg := range expectLogMessage {
			if got := fake01.Pop(); expectMsg != got {
				t.Errorf("Expect fake01.Pop() is %v, actual is %v", expectMsg, got)
			}
			if got := fake02.Pop(); expectMsg != got {
				t.Errorf("Expect fake02.Pop() is %v, actual is %v", expectMsg, got)
			}
		}
		if got := fake01.Count(); got != 0 {
			t.Errorf("Expect fake01.Count() is %v, actual is %v", 0, got)
		}
		if got := fake02.Count(); got != 0 {
			t.Errorf("Expect fake02.Count() is %v, actual is %v", 0, got)
		}
	}

	{
		SetDefaultTargets([]Target{})
		h := NewHandler()
		h.BindTimeFmt(func(*strings.Builder, time.Time) {})
		h.DBG().
			P("xing xi").
			Z()

		if got := fake01.Count(); got != 0 {
			t.Errorf("Expect fake01.Count() is %v, actual is %v", 0, got)
		}
		if got := fake02.Count(); got != 0 {
			t.Errorf("Expect fake02.Count() is %v, actual is %v", 0, got)
		}
	}

}

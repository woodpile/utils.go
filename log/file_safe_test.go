package log

import (
	"os"
	"sync"
	"testing"
)

func TestSomeFileClose(t *testing.T) {
	tf, err := MakeTargetFile(os.TempDir(), "file_in_logs.log")
	if nil != err {
		t.Errorf("make target file, %v", err)
		return
	}

	count := 16
	arrLog := make([]LoggerHandler, 0, 32)
	for i := 0; i < count; i++ {
		l := NewHandlerWithConfig(globalHandlerConfig)
		arrLog = append(arrLog, l)
		l.AddTarget(tf)
	}

	var wg sync.WaitGroup
	for i := 0; i < count; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			arrLog[i].DBG().
				PF("abc").
				Z()
			arrLog[i].DBG().
				PF("def").
				Z()
			arrLog[i].DBG().
				PF("ghi").
				Z()
			arrLog[i].Release()
		}(i)
	}

	wg.Wait()
}

package main

import (
	"fmt"
	"net/http"
	_ "net/http/pprof"
	"os"
	"runtime"
	"sync"

	"gitee.com/woodpile/utils.go/log"
)

func main() {
	// tf, err := log.MakeTargetFile(os.TempDir(), "file_in_logs.log")
	tf, err := log.MakeTargetFileRotate(&log.RotateConfig{
		Dir:    os.TempDir(),
		Prefix: "utils.log.pprof",
		Ext:    "log",

		RotateDuring: 60,
		RotateSize:   1024,
	})
	if nil != err {
		fmt.Printf("Error: make target file, %v", err)
		return
	}

	go http.ListenAndServe(":29990", nil)

	i1 := int32(0)
	fmt.Scan(&i1)

	count := 512
	arrLog := make([]log.LoggerHandler, 0, 32)
	for i := 0; i < count; i++ {
		l := log.NewHandler()
		arrLog = append(arrLog, l)
		l.AddTarget(tf)
	}

	var wg sync.WaitGroup
	for i := 0; i < count; i++ {
		wg.Add(1)
		go func(i int) {
			defer wg.Done()
			arrLog[i].DBG().
				PF("abc").
				Z()
			arrLog[i].DBG().
				PF("def").
				Z()
			arrLog[i].DBG().
				PF("ghi").
				Z()
			arrLog[i].Release()
		}(i)
	}

	wg.Wait()

	runtime.GC()

	i2 := int32(0)
	fmt.Scan(&i2)
}

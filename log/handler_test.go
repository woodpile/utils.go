package log

import (
	"reflect"
	"strings"
	"testing"
	"time"
)

func _utGetEmptyHandlerCommon() *HandlerCommon {
	return &HandlerCommon{
		Sep:       "|",
		SepFmt:    "=",
		TimeFmt:   nil,
		Targets:   append(make([]Target, 0, 4), defaultTargets...),
		Prefixes:  make([][2]string, 0, 4),
		PrefixStr: "",
		BCodeLine: false,
	}
}

func TestNewHandler(t *testing.T) {
	tests := []struct {
		name string
		want LoggerHandler
	}{
		//没有测试意义
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := NewHandler(); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewHandler() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_NewHandlerWithConfig(t *testing.T) {
	type args struct {
		cfg *HandlerConfig
	}
	tests := []struct {
		name string
		args args
		want *Handler
	}{
		{
			name: "default.config",
			args: args{
				cfg: &HandlerConfig{
					Level:            LvlInfo,
					Seperator:        "|",
					FmtSeperator:     "=",
					BCodeLine:        false,
					BUseJSONAssemble: false,
					ContextPoolSize:  4,
				},
			},
			want: &Handler{
				common: &HandlerCommon{
					Sep:       "|",
					SepFmt:    "=",
					TimeFmt:   nil,
					Targets:   append(make([]Target, 0, 4), defaultTargets...),
					Prefixes:  make([][2]string, 0, 4),
					PrefixStr: "",
					BCodeLine: false,
				},

				level: LvlInfo,
				ctxPool: NewContextPool(&HandlerCommon{
					Sep:       "|",
					SepFmt:    "=",
					TimeFmt:   nil,
					Targets:   append(make([]Target, 0, 4), defaultTargets...),
					Prefixes:  make([][2]string, 0, 4),
					PrefixStr: "",
					BCodeLine: false,
				}, 4),
				ctxPoolSize: 4,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewHandlerWithConfig(tt.args.cfg)
			got.common.TimeFmt = nil
			got.ctxPool.pool = nil
			tt.want.ctxPool.pool = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("defaultConfiguredHandler(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_Clone(t *testing.T) {
	handler01, fake01 := makeHandlerWithFakeTargetOnly(nil)
	handler01.SetLevel(LvlWarning)
	handler01.AddPrefix("1", "pipi")
	handler01.AddPrefix("2", "mimi")
	handler01.Ctx().FS("KeyA", "ValB").FU("NumA", 1230).P("popo").Z()
	handler01out := fake01.Pop()
	if handler01out == "" {
		t.Errorf("Handler.Clone(), prepare want message failed")
	}

	tests := []struct {
		name string
		h    *Handler
		want string
	}{
		{
			name: "01",
			h:    handler01,
			want: handler01out,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cl := tt.h.Clone()
			cl.Ctx().FS("KeyA", "ValB").FU("NumA", 1230).P("popo").Z()
			if got := fake01.Pop(); got != tt.want {
				t.Errorf("Handler.Clone(), message %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_BindTimeFmt(t *testing.T) {
	handler01, fake := makeHandlerWithFakeTargetOnly(nil)
	handler01.SetLevel(LvlDebug)

	type args struct {
		f FuncTimeFmt
	}
	tests := []struct {
		name string
		h    *Handler
		args args
		want string
	}{
		{
			name: "00",
			h:    handler01,
			args: args{f: func(buff *strings.Builder, t time.Time) { buff.WriteString("biubiubiu") }},
			want: "biubiubiu|DBG|helloworld",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.h.BindTimeFmt(tt.args.f)
			tt.h.Ctx().P("helloworld").Z()
			if got := fake.Pop(); got != tt.want {
				t.Errorf("after Handler.BindTimeFmt(), p() got message %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_SetSeperator(t *testing.T) {
	handler01, fake := makeHandlerWithFakeTargetOnly(nil)
	handler01.SetLevel(LvlDebug)

	type args struct {
		sep string
	}
	tests := []struct {
		name string
		h    *Handler
		args args
		want string
	}{
		{
			name: "00",
			h:    handler01,
			args: args{sep: "$$"},
			want: "$$DBG$$hello$$world",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.h.SetSeperator(tt.args.sep)
			tt.h.Ctx().P("hello").P("world").Z()
			if got := fake.Pop(); got != tt.want {
				t.Errorf("after Handler.SetSeperator(), p() got message %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_Level(t *testing.T) {
	handler01 := NewHandlerWithConfig(GlobalHandlerConfig())
	handler02 := NewHandlerWithConfig(GlobalHandlerConfig())
	handler02.SetLevel(LvlWarning)

	tests := []struct {
		name string
		h    *Handler
		want int
	}{
		{
			name: "00",
			h:    handler01,
			want: GlobalLevel(),
		},
		{
			name: "01",
			h:    handler02,
			want: LvlWarning,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.h.Level(); got != tt.want {
				t.Errorf("Handler.Level() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_SetLevel(t *testing.T) {
	handler01 := NewHandlerWithConfig(GlobalHandlerConfig())
	handler01.SetLevel(LvlDebug)

	type args struct {
		l int
	}
	tests := []struct {
		name string
		h    *Handler
		args args
		want int
	}{
		{
			name: "01.01",
			h:    handler01,
			args: args{l: LvlError},
			want: LvlError,
		},
		{
			name: "01.02",
			h:    handler01,
			args: args{l: LvlDebug},
			want: LvlDebug,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.h.SetLevel(tt.args.l)
			if got := tt.h.Level(); got != tt.want {
				t.Errorf("Handler.SetLevel(), lv %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_AddTarget(t *testing.T) {
	handler01 := NewHandlerWithConfig(GlobalHandlerConfig())
	type args struct {
		t Target
	}
	tests := []struct {
		name string
		h    *Handler
		args args
		want int
	}{
		{
			name: "01.01",
			h:    handler01,
			args: args{t: Stdout},
			want: 2,
		},
		{
			name: "01.02",
			h:    handler01,
			args: args{t: &FakeTarget{}},
			want: 3,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.h.AddTarget(tt.args.t)
			if got := len(tt.h.common.Targets); got != tt.want {
				t.Errorf("Handler.AddTarget(), got len %v, want len %v", got, tt.want)
			}
		})
	}
}

func TestHandler_DelTarget(t *testing.T) {
	handler01 := NewHandlerWithConfig(GlobalHandlerConfig())
	handler01.AddTarget(&FakeTarget{})
	type args struct {
		name string
	}
	tests := []struct {
		name string
		h    *Handler
		args args
		want int
	}{
		{
			name: "01.01",
			h:    handler01,
			args: args{name: "stdout"},
			want: 1,
		},
		{
			name: "01.02",
			h:    handler01,
			args: args{name: "fake"},
			want: 0,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.h.DelTarget(tt.args.name)
			if got := len(tt.h.common.Targets); got != tt.want {
				t.Errorf("Handler.DelTarget(), got len %v, want len %v", got, tt.want)
			}
		})
	}
}

func TestHandler_AddPrefix(t *testing.T) {
	handler01 := NewHandlerWithConfig(GlobalHandlerConfig())
	handler01.SetLevel(LvlDebug)

	type args struct {
		key string
		val string
	}
	tests := []struct {
		name string
		h    *Handler
		args args
		want string
	}{
		{
			name: "01.01",
			h:    handler01,
			args: args{"1", "pipi"},
			want: "|pipi",
		},
		{
			name: "01.02",
			h:    handler01,
			args: args{"2", "lala"},
			want: "|pipi|lala",
		},
		{
			name: "01.03",
			h:    handler01,
			args: args{"1", "haha"},
			want: "|lala|haha",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.h.AddPrefix(tt.args.key, tt.args.val)
			if got := tt.h.common.PrefixStr; got != tt.want {
				t.Errorf("Handler.AddPrefix(), result got %v, want %v, -- %+v", got, tt.want, tt.h.common.Prefixes)
			}
		})
	}
}

func TestHandler_DelPrefix(t *testing.T) {
	handler01 := NewHandlerWithConfig(GlobalHandlerConfig())
	handler01.SetLevel(LvlDebug)
	handler01.AddPrefix("1", "pipi")
	handler01.AddPrefix("2", "lala")
	type args struct {
		key string
	}
	tests := []struct {
		name string
		h    *Handler
		args args
		want string
	}{
		{
			name: "01",
			h:    handler01,
			args: args{"1"},
			want: "|lala",
		},
		{
			name: "02",
			h:    handler01,
			args: args{"2"},
			want: "",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.h.DelPrefix(tt.args.key)
			if got := tt.h.common.PrefixStr; got != tt.want {
				t.Errorf("Handler.DelPrefix(), result got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_SwitchCodeLine(t *testing.T) {
	handler01, fake01 := makeHandlerWithFakeTargetOnly(nil)
	type args struct {
		b bool
	}
	tests := []struct {
		name  string
		h     *Handler
		args  args
		sufdo func(h *Handler)
		want  string
	}{
		{
			name: "01",
			h:    handler01,
			args: args{b: false},
			sufdo: func(h *Handler) {
				h.Ctx().P("haha").Z()
			},
			want: "|DBG|haha",
		},
		{
			name: "02",
			h:    handler01,
			args: args{b: true},
			sufdo: func(h *Handler) {
				h.Ctx().P("haha").Z()
			},
			want: "|DBG|handler_test.go:433|haha",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.h.SwitchCodeLine(tt.args.b)
			tt.sufdo(tt.h)
			if got := fake01.Pop(); got != tt.want {
				t.Errorf("Handler.SwitchCodeLine(), message %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_updatePrefixString(t *testing.T) {
	handler01 := NewHandlerWithConfig(GlobalHandlerConfig())
	handler01.SetLevel(LvlDebug)
	tests := []struct {
		name string
		h    *Handler
		do   func(h *Handler)
		want string
	}{
		{
			name: "01",
			h:    handler01,
			do: func(h *Handler) {
				h.AddPrefix("1", "pipi")
			},
			want: "|pipi",
		},
		{
			name: "02",
			h:    handler01,
			do: func(h *Handler) {
				h.AddPrefix("2", "lala")
			},
			want: "|pipi|lala",
		},
		{
			name: "03",
			h:    handler01,
			do: func(h *Handler) {
				h.SetSeperator(")")
			},
			want: ")pipi)lala",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.do(tt.h)
			tt.h.common.updatePrefixString()
			if got := tt.h.common.PrefixStr; got != tt.want {
				t.Errorf("Handler.updatePrefixString() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_getContext(t *testing.T) {
	handler01 := NewHandlerWithConfig(GlobalHandlerConfig())

	tests := []struct {
		name string
		h    *Handler
		want Context
	}{
		{
			name: "ok.0",
			h:    handler01,
			want: _utGetEmptyContextBase(handler01.common, handler01.Level()),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.h.getContext(handler01.Level())
			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Handler.getContext() = %+v, want %+v", got, tt.want)
			}
		})
	}
}

func TestHandler_Ctx(t *testing.T) {
	handler01 := NewHandlerWithConfig(GlobalHandlerConfig())

	tests := []struct {
		name string
		h    *Handler
		want Context
	}{
		{
			name: "ok.0",
			h:    handler01,
			want: _utGetEmptyContextBase(handler01.common, handler01.Level()),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.h.Ctx()
			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Handler.Ctx() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_TRC(t *testing.T) {
	handler01, fake01 := makeHandlerWithFakeTargetOnly(nil)
	handler01.SetLevel(LvlDebug)

	SetGlobalLevel(LvlTrace)
	defer SetGlobalLevel(LvlDebug)

	type want struct {
		msg string
	}
	tests := []struct {
		name  string
		h     *Handler
		sufdo func(ctx Context)
		want  want
	}{
		{
			name:  "01",
			h:     handler01,
			sufdo: func(ctx Context) { ctx.P("haha").Z() },
			want:  want{msg: "|TRC|haha"},
		},
		{
			name:  "02",
			h:     handler01,
			sufdo: func(ctx Context) { ctx.Z() },
			want:  want{msg: "|TRC"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := tt.h.TRC()
			tt.sufdo(ctx)
			if got := fake01.Pop(); got != tt.want.msg {
				t.Errorf("Handler.TRC(), message %v, want %v", got, tt.want.msg)
			}
		})
	}
}

func TestHandler_DBG(t *testing.T) {
	handler01, fake01 := makeHandlerWithFakeTargetOnly(nil)
	handler01.SetLevel(LvlDebug)

	type want struct {
		msg string
	}
	tests := []struct {
		name  string
		h     *Handler
		sufdo func(ctx Context)
		want  want
	}{
		{
			name:  "01",
			h:     handler01,
			sufdo: func(ctx Context) { ctx.P("haha").Z() },
			want:  want{msg: "|DBG|haha"},
		},
		{
			name:  "02",
			h:     handler01,
			sufdo: func(ctx Context) { ctx.Z() },
			want:  want{msg: "|DBG"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := tt.h.DBG()
			tt.sufdo(ctx)
			if got := fake01.Pop(); got != tt.want.msg {
				t.Errorf("Handler.DBG(), message %v, want %v", got, tt.want.msg)
			}
		})
	}
}

func TestHandler_INF(t *testing.T) {
	handler01, fake01 := makeHandlerWithFakeTargetOnly(nil)
	handler01.SetLevel(LvlDebug)

	type want struct {
		msg string
	}
	tests := []struct {
		name  string
		h     *Handler
		sufdo func(ctx Context)
		want  want
	}{
		{
			name:  "01",
			h:     handler01,
			sufdo: func(ctx Context) { ctx.P("haha").Z() },
			want:  want{msg: "|INF|haha"},
		},
		{
			name:  "02",
			h:     handler01,
			sufdo: func(ctx Context) { ctx.Z() },
			want:  want{msg: "|INF"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := tt.h.INF()
			tt.sufdo(ctx)
			if got := fake01.Pop(); got != tt.want.msg {
				t.Errorf("Handler.INF(), message %v, want %v", got, tt.want.msg)
			}
		})
	}
}

func TestHandler_WAR(t *testing.T) {
	handler01, fake01 := makeHandlerWithFakeTargetOnly(nil)
	handler01.SetLevel(LvlDebug)

	type want struct {
		msg string
	}
	tests := []struct {
		name  string
		h     *Handler
		sufdo func(ctx Context)
		want  want
	}{
		{
			name:  "01",
			h:     handler01,
			sufdo: func(ctx Context) { ctx.P("haha").Z() },
			want:  want{msg: "|WAR|haha"},
		},
		{
			name:  "02",
			h:     handler01,
			sufdo: func(ctx Context) { ctx.Z() },
			want:  want{msg: "|WAR"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := tt.h.WAR()
			tt.sufdo(ctx)
			if got := fake01.Pop(); got != tt.want.msg {
				t.Errorf("Handler.WAR(), message %v, want %v", got, tt.want.msg)
			}
		})
	}
}

func TestHandler_ERR(t *testing.T) {
	handler01, fake01 := makeHandlerWithFakeTargetOnly(nil)
	handler01.SetLevel(LvlDebug)

	type want struct {
		msg string
	}
	tests := []struct {
		name  string
		h     *Handler
		sufdo func(ctx Context)
		want  want
	}{
		{
			name:  "01",
			h:     handler01,
			sufdo: func(ctx Context) { ctx.P("haha").Z() },
			want:  want{msg: "|ERR|haha"},
		},
		{
			name:  "02",
			h:     handler01,
			sufdo: func(ctx Context) { ctx.Z() },
			want:  want{msg: "|ERR"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := tt.h.ERR()
			tt.sufdo(ctx)
			if got := fake01.Pop(); got != tt.want.msg {
				t.Errorf("Handler.ERR(), message %v, want %v", got, tt.want.msg)
			}
		})
	}
}

func TestHandler_FAT(t *testing.T) {
	handler01, fake01 := makeHandlerWithFakeTargetOnly(nil)
	handler01.SetLevel(LvlDebug)

	type want struct {
		msg string
	}
	tests := []struct {
		name  string
		h     *Handler
		sufdo func(ctx Context)
		want  want
	}{
		{
			name:  "01",
			h:     handler01,
			sufdo: func(ctx Context) { ctx.P("haha").Z() },
			want:  want{msg: "|FAT|haha"},
		},
		{
			name:  "02",
			h:     handler01,
			sufdo: func(ctx Context) { ctx.Z() },
			want:  want{msg: "|FAT"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ctx := tt.h.FAT()
			tt.sufdo(ctx)
			if got := fake01.Pop(); got != tt.want.msg {
				t.Errorf("Handler.FAT(), message %v, want %v", got, tt.want.msg)
			}
		})
	}
}

func TestHandlerCommon_updatePrefixString(t *testing.T) {
	tests := []struct {
		name string
		h    *HandlerCommon
		want string
	}{
		{
			name: "ok.0",
			h:    utHandlerCommon01,
			want: "",
		},
		{
			name: "ok.1",
			h: func() *HandlerCommon {
				h := NewHandlerWithConfig(globalHandlerConfig)
				h.AddPrefix("k1", "value1")
				return h.common
			}(),
			want: "|value1",
		},
		{
			name: "ok.2",
			h: func() *HandlerCommon {
				h := NewHandlerWithConfig(globalHandlerConfig)
				h.AddPrefix("k1", "value1")
				h.AddPrefix("k2", "value2")
				return h.common
			}(),
			want: "|value1|value2",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.h.updatePrefixString()
			if got := tt.h.PrefixStr; got != tt.want {
				t.Errorf("updatePrefixString(). got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestNewHandlerWithConfig(t *testing.T) {
	type args struct {
		cfg *HandlerConfig
	}
	tests := []struct {
		name string
		args args
		want *Handler
	}{
		{
			name: "ok.0",
			args: args{
				cfg: globalHandlerConfig,
			},
			want: &Handler{
				common: &HandlerCommon{
					Sep:       "|",
					SepFmt:    "=",
					TimeFmt:   nil,
					Targets:   append(make([]Target, 0, 4), defaultTargets...),
					Prefixes:  make([][2]string, 0, 4),
					PrefixStr: "",

					BCodeLine:       false,
					UseJSONAssemble: false,
				},

				level: LvlDebug,

				ctxPoolSize: 4,
				ctxPool: &ContextPool{
					common: &HandlerCommon{
						Sep:       "|",
						SepFmt:    "=",
						TimeFmt:   nil,
						Targets:   append(make([]Target, 0, 4), defaultTargets...),
						Prefixes:  make([][2]string, 0, 4),
						PrefixStr: "",

						BCodeLine:       false,
						UseJSONAssemble: false,
					},
					pool: nil,
				},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewHandlerWithConfig(tt.args.cfg)
			got.ctxPool.pool = nil
			got.common.TimeFmt = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewHandlerWithConfig() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestHandler_SwitchJSONAssemble(t *testing.T) {
	type args struct {
		b bool
	}
	tests := []struct {
		name string
		h    *Handler
		args args
	}{
		//no need test
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.h.SwitchJSONAssemble(tt.args.b)
		})
	}
}

func TestHandler_Release(t *testing.T) {
	tests := []struct {
		name string
		h    *Handler
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.h.Release()
		})
	}
}

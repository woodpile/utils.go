package log

import (
	"strings"
	"testing"
	"time"
)

func Test_assembleAsLineSeperator(t *testing.T) {
	type args struct {
		ctx *ContextBase
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok.0",
			args: args{
				ctx: func() *ContextBase {
					common := &HandlerCommon{
						Sep:     "|",
						SepFmt:  "=",
						TimeFmt: func(*strings.Builder, time.Time) {},
						Targets: append(make([]Target, 0, 4), defaultTargets...),
						Prefixes: [][2]string{
							{"k1", "v1"},
							{"k2", "v2"},
						},
						PrefixStr:       "",
						BCodeLine:       false,
						UseJSONAssemble: false,
					}
					common.updatePrefixString()
					ctx := NewContextBase(common, LvlDebug)
					ctx.Pre("pre1")
					ctx.FU("u1", 123)
					ctx.FS("s1", "str1")
					ctx.PF("--%s--", "::")
					return ctx
				}(),
			},
			want: "|DBG|v1|v2|pre1|u1=123|s1=str1|--::--",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := assembleAsLineSeperator(tt.args.ctx); got != tt.want {
				t.Errorf("assembleAsLineSeperator() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_assembleAsJSON(t *testing.T) {
	type args struct {
		ctx *ContextBase
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok.0",
			args: args{
				ctx: func() *ContextBase {
					common := &HandlerCommon{
						Sep:     "|",
						SepFmt:  "=",
						TimeFmt: func(*strings.Builder, time.Time) {},
						Targets: append(make([]Target, 0, 4), defaultTargets...),
						Prefixes: [][2]string{
							{"k1", "v1"},
							{"k2", "v2"},
						},
						PrefixStr:       "",
						BCodeLine:       false,
						UseJSONAssemble: false,
					}
					common.updatePrefixString()
					ctx := NewContextBase(common, LvlDebug)
					ctx.Pre("pre1")
					ctx.FU("u1", 123)
					ctx.FS("s1", "str1")
					ctx.PF("--%s--", "::")
					return ctx
				}(),
			},
			want: `{"time":"","lv":"DBG","p":{"k1":"v1","k2":"v2"},"cp":["pre1"],"cf":{"u1":123,"s1":"str1"},"cff":{},"ms":["--::--"]}`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := assembleAsJSON(tt.args.ctx); got != tt.want {
				t.Errorf("assembleAsJSON() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_assembleValueAsJSON(t *testing.T) {
	type args struct {
		buff *strings.Builder
		v    interface{}
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		{
			name: "ok.0",
			args: args{
				buff: &strings.Builder{},
				v:    "abc",
			},
			want: `"abc"`,
		},
		{
			name: "ok.1",
			args: args{
				buff: &strings.Builder{},
				v:    `{"abc":"def","cdf":123,"ghi":[3,"abc"]}`,
			},
			want: `"{\"abc\":\"def\",\"cdf\":123,\"ghi\":[3,\"abc\"]}"`,
		},
		{
			name: "ok.2",
			args: args{
				buff: &strings.Builder{},
				v:    `"{\"abc\":\"def\",\"cdf\":123,\"ghi\":[3,\"abc\"]}"`,
			},
			want: `"\"{\\\"abc\\\":\\\"def\\\",\\\"cdf\\\":123,\\\"ghi\\\":[3,\\\"abc\\\"]}\""`,
		},
		{
			name: "ok.3",
			args: args{
				buff: &strings.Builder{},
				v:    123,
			},
			want: `123`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			assembleValueAsJSON(tt.args.buff, tt.args.v)
			if got := tt.args.buff.String(); got != tt.want {
				t.Errorf("assembleValueAsJSON(). got = %v, want %v", got, tt.want)
			}
		})
	}
}

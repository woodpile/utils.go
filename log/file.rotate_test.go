package log

import (
	"os"
	"reflect"
	"testing"
	"time"
)

var (
	utRotateConfig = &RotateConfig{
		Dir:    os.TempDir(),
		Prefix: "utils.log.ut",
		Ext:    "log",

		RotateDuring: 60,
		RotateSize:   1024,
	}
)

func TestMakeTargetFileRotate(t *testing.T) {
	t.Logf("TempDir: %v", os.TempDir())
	now := time.Now()

	type args struct {
		cfg *RotateConfig
	}
	tests := []struct {
		name    string
		args    args
		want    *TargetFileRotate
		wantErr bool
	}{
		{
			name: "ok.0",
			args: args{
				cfg: utRotateConfig,
			},
			want: &TargetFileRotate{
				cfg: utRotateConfig,

				levels: allLevels,
				using:  0,

				file: nil,

				lastRotateTime: time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), 0, 0, now.Location()).Unix(),
				rotateDuring:   60,

				stackSize:  0,
				rotateSize: 1024,
				sizeIndex:  0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := MakeTargetFileRotate(tt.args.cfg)
			if (err != nil) != tt.wantErr {
				t.Errorf("MakeTargetFileRotate() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			got.file = nil
			tt.want.file = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("MakeTargetFileRotate() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTargetFileRotate_Name(t *testing.T) {
	tests := []struct {
		name string
		tr   *TargetFileRotate
		want string
	}{
		{
			name: "ok.0",
			tr: func() *TargetFileRotate {
				tr, _ := MakeTargetFileRotate(utRotateConfig)
				return tr
			}(),
			want: "file.utils.log.ut",
		},
		{
			name: "ok.1",
			tr: func() *TargetFileRotate {
				tr, _ := MakeTargetFileRotate(&RotateConfig{
					Dir:    os.TempDir(),
					Prefix: "ball",

					RotateDuring: 60,
					RotateSize:   1024,
				})
				return tr
			}(),
			want: "file.ball",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tr.Name(); got != tt.want {
				t.Errorf("TargetFileRotate.Name() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTargetFileRotate_IncrUsing(t *testing.T) {
	tests := []struct {
		name string
		tr   *TargetFileRotate
		want int32
	}{
		{
			name: "ok.0",
			tr: func() *TargetFileRotate {
				tr, _ := MakeTargetFileRotate(utRotateConfig)
				return tr
			}(),
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.tr.IncrUsing()
			if got := tt.tr.using; got != tt.want {
				t.Errorf("TargetFileRotate.IncrUsing(). got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTargetFileRotate_IsIgnore(t *testing.T) {
	type args struct {
		l int
	}
	tests := []struct {
		name string
		tr   *TargetFileRotate
		args args
		want bool
	}{
		{
			name: "ok.0",
			tr: func() *TargetFileRotate {
				tr, _ := MakeTargetFileRotate(utRotateConfig)
				tr.SetLevels([]int{LvlDebug})
				return tr
			}(),
			args: args{
				l: LvlDebug,
			},
			want: false,
		},
		{
			name: "ok.1",
			tr: func() *TargetFileRotate {
				tr, _ := MakeTargetFileRotate(utRotateConfig)
				tr.SetLevels([]int{LvlDebug, LvlError})
				return tr
			}(),
			args: args{
				l: LvlDebug,
			},
			want: false,
		},
		{
			name: "ok.2",
			tr: func() *TargetFileRotate {
				tr, _ := MakeTargetFileRotate(utRotateConfig)
				tr.SetLevels([]int{LvlDebug, LvlError})
				return tr
			}(),
			args: args{
				l: LvlWarning,
			},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tr.IsIgnore(tt.args.l); got != tt.want {
				t.Errorf("TargetFileRotate.IsIgnore() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTargetFileRotate_SetLevels(t *testing.T) {
	type args struct {
		ls []int
	}
	tests := []struct {
		name string
		tr   *TargetFileRotate
		args args
	}{
		// test in IsIgnore function
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.tr.SetLevels(tt.args.ls)
		})
	}
}

func TestTargetFileRotate_Put(t *testing.T) {
	type args struct {
		msg string
	}
	tests := []struct {
		name string
		tr   *TargetFileRotate
		args args
	}{
		{
			name: "ok.0",
			tr: func() *TargetFileRotate {
				tr, _ := MakeTargetFileRotate(utRotateConfig)
				return tr
			}(),
			args: args{
				msg: "something",
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.tr.Put(tt.args.msg)
		})
	}
}

func TestTargetFileRotate_Close(t *testing.T) {
	tests := []struct {
		name string
		tr   *TargetFileRotate
	}{
		{
			name: "ok.0",
			tr: func() *TargetFileRotate {
				tr, _ := MakeTargetFileRotate(utRotateConfig)
				return tr
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.tr.Close()
		})
	}
}

func TestTargetFileRotate_genFileName(t *testing.T) {
	type args struct {
		ts int64
	}
	tests := []struct {
		name string
		tr   *TargetFileRotate
		args args
		want string
	}{
		{
			name: "ok.0",
			tr: func() *TargetFileRotate {
				tr, _ := MakeTargetFileRotate(utRotateConfig)
				return tr
			}(),
			args: args{
				ts: time.Date(2021, 1, 2, 23, 45, 0, 0, time.Local).Unix(),
			},
			want: "utils.log.ut.2021-01-02-234500.0.log",
		},
		{
			name: "ok.1",
			tr: func() *TargetFileRotate {
				tr, _ := MakeTargetFileRotate(utRotateConfig)
				tr.sizeIndex = 17
				return tr
			}(),
			args: args{
				ts: time.Date(2021, 1, 2, 23, 45, 0, 0, time.Local).Unix(),
			},
			want: "utils.log.ut.2021-01-02-234500.17.log",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.tr.genFileName(tt.args.ts); got != tt.want {
				t.Errorf("TargetFileRotate.genFileName() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTargetFileRotate_tryRotateFile(t *testing.T) {
	tests := []struct {
		name string
		tr   *TargetFileRotate
	}{
		{
			name: "ok.0",
			tr: func() *TargetFileRotate {
				tr, _ := MakeTargetFileRotate(&RotateConfig{
					Dir:    os.TempDir(),
					Prefix: "utils.log.ut.rotate",
					Ext:    "log",

					RotateDuring: 60,
					RotateSize:   1024,
				})
				tr.stackSize = 2048
				return tr
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.tr.tryRotateFile(5)
		})
	}
}

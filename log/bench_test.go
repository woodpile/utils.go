package log

import (
	"strconv"
	"strings"
	"testing"
	"time"
)

func BenchmarkLogPrint(b *testing.B) {
	handler := NewHandler()
	handler.AddPrefix("service", "LogBench")
	{
		handler.DelTarget("stdout")
		fake := &FakeTarget{}
		fake.SetNotCache()
		handler.AddTarget(fake)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		handler.INF().
			Pre("Temp").
			// FX("uuid", 0x010082).
			FS("key", "value").
			FU("player", 9901).
			P("this is a message not long and not short").Z()
	}
	b.StopTimer()
}

func BenchmarkLogLevelIgnore(b *testing.B) {
	handler := NewHandler()
	handler.AddPrefix("service", "LogBench")
	{
		fake := &FakeTarget{}
		fake.SetNotCache()
		handler.AddTarget(fake)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		handler.TRC().
			Pre("Temp").
			// FX("uuid", 0x010082).
			FS("key", "value").
			FU("player", 9901).
			P("this is a message not long and not short").Z()
	}
	b.StopTimer()
}

func BenchmarkLoggerClone(b *testing.B) {
	handler := NewHandler()
	handler.AddPrefix("service", "LogBench")
	{
		fake := &FakeTarget{}
		fake.SetNotCache()
		handler.AddTarget(fake)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		nl := handler.Clone()
		nl.AddPrefix("player", "PlayerID.9001")
	}
	b.StopTimer()
}

func BenchmarkLogPrintWithNoTime(b *testing.B) {
	handler := NewHandler()
	handler.BindTimeFmt(func(*strings.Builder, time.Time) {})
	handler.AddPrefix("service", "LogBench")
	{
		handler.DelTarget("stdout")
		fake := &FakeTarget{}
		fake.SetNotCache()
		handler.AddTarget(fake)
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		handler.INF().
			Pre("Temp").
			// FX("uuid", 0x010082).
			FS("key", "value").
			FU("player", 9901).
			P("this is a message not long and not short").Z()
	}
	b.StopTimer()
}

func BenchmarkLogPrintJSON(b *testing.B) {
	handler := NewHandler()
	handler.AddPrefix("service", "LogBench")
	{
		handler.DelTarget("stdout")
		fake := &FakeTarget{}
		fake.SetNotCache()
		handler.AddTarget(fake)
	}
	handler.SwitchJSONAssemble(true)

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		handler.INF().
			Pre("Temp").
			// FX("uuid", 0x010082).
			FS("key", "value").
			FU("player", 9901).
			P("this is a message not long and not short").Z()
	}
	b.StopTimer()
}

type STOne struct {
	Int1 int64
	F2   float64
	Str3 string
	Arr4 []int64
}

const (
	keyInt1 = "int1"
	keyF2   = "f2"
	keyStr3 = "str3"
	keyArr4 = "arr4"
)

func (st *STOne) AssembleAsLineSeperator(buff *strings.Builder, com *HandlerCommon) {
	buff.WriteByte('{')
	buff.WriteString(strconv.FormatInt(st.Int1, 10))
	buff.WriteByte(',')
	buff.WriteString(strconv.FormatFloat(st.F2, 'g', 6, 64))
	buff.WriteByte(',')
	buff.WriteString(st.Str3)
	buff.WriteByte(',')
	buff.WriteByte('[')
	for i := 0; i < len(st.Arr4); i++ {
		buff.WriteString(strconv.FormatInt(st.Arr4[i], 10))
		if i < len(st.Arr4)-1 {
			buff.WriteByte(',')
		}
	}
	buff.WriteByte(']')
	buff.WriteByte('}')
}

func (st *STOne) AssembleAsJSON(buff *strings.Builder, com *HandlerCommon) {
	buff.WriteByte('{')
	buff.WriteByte('"')
	buff.WriteString(keyInt1)
	buff.WriteByte('"')
	buff.WriteByte(':')
	buff.WriteString(strconv.FormatInt(st.Int1, 10))
	buff.WriteByte(',')
	buff.WriteByte('"')
	buff.WriteString(keyF2)
	buff.WriteByte('"')
	buff.WriteByte(':')
	buff.WriteString(strconv.FormatFloat(st.F2, 'g', 6, 64))
	buff.WriteByte(',')
	buff.WriteByte('"')
	buff.WriteString(keyStr3)
	buff.WriteByte('"')
	buff.WriteByte(':')
	buff.WriteByte('"')
	buff.WriteString(st.Str3)
	buff.WriteByte('"')
	buff.WriteByte(',')
	buff.WriteByte('[')
	for i := 0; i < len(st.Arr4); i++ {
		buff.WriteString(strconv.FormatInt(st.Arr4[i], 10))
		if i < len(st.Arr4)-1 {
			buff.WriteByte(',')
		}
	}
	buff.WriteByte(']')
	buff.WriteByte('}')
}

func BenchmarkLogPrintWithStruct(b *testing.B) {
	handler := NewHandler()
	handler.AddPrefix("service", "LogBench")
	{
		handler.DelTarget("stdout")
		fake := &FakeTarget{}
		fake.SetNotCache()
		handler.AddTarget(fake)
	}

	st1 := &STOne{
		Int1: 123,
		F2:   3.3,
		Str3: "hello",
		Arr4: []int64{7, 8, 9},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		handler.INF().
			Pre("Temp").
			// FX("uuid", 0x010082).
			FS("key", "value").
			FU("player", 9901).
			FT("st1", st1).
			P("this is a message not long and not short").Z()
	}
	b.StopTimer()
}

func BenchmarkLogPrintJSONWithStruct(b *testing.B) {
	handler := NewHandler()
	handler.AddPrefix("service", "LogBench")
	{
		handler.DelTarget("stdout")
		fake := &FakeTarget{}
		fake.SetNotCache()
		handler.AddTarget(fake)
	}
	handler.SwitchJSONAssemble(true)

	st1 := &STOne{
		Int1: 123,
		F2:   3.3,
		Str3: "hello",
		Arr4: []int64{7, 8, 9},
	}

	b.ResetTimer()
	for i := 0; i < b.N; i++ {
		handler.INF().
			Pre("Temp").
			// FX("uuid", 0x010082).
			FS("key", "value").
			FU("player", 9901).
			FT("st1", st1).
			P("this is a message not long and not short").Z()
	}
	b.StopTimer()
}

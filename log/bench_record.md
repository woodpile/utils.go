# bench record

Run command: `go test -bench "Benchmark*" -benchtime 3s -benchmem`

## Record

### Commit:67688299134d87b03785bbbe32fd3980bdf547cd Check at "Wed, 03 Feb 2021 16:34:55 +0800" 不含"FX()" "稍好一些的机器"

```text
goos: windows
goarch: amd64
pkg: gitee.com/woodpile/utils.go/log
BenchmarkLogPrint-8                      2967369              1217 ns/op             262 B/op          6 allocs/op
BenchmarkLogLevelIgnore-8               378759742                9.45 ns/op            0 B/op          0 allocs/op
BenchmarkLoggerClone-8                   6648670               544 ns/op             480 B/op         10 allocs/op
BenchmarkLogPrintWithNoTime-8            5165598               696 ns/op             272 B/op          7 allocs/op
BenchmarkLogPrintJSON-8                  1545339              2336 ns/op             727 B/op         18 allocs/op
BenchmarkLogPrintWithStruct-8            2183728              1647 ns/op             558 B/op         10 allocs/op
BenchmarkLogPrintJSONWithStruct-8        1371399              2634 ns/op             766 B/op         21 allocs/op
```

### Commit:ae9a3ca86e6b75a338797f01d1fdfa5a15855879 Check at "Fri, 01 Jan 2021 21:58:28 +0800" 不含"FX()"

```text
goos: windows
goarch: amd64
pkg: gitee.com/woodpile/utils.go/log
BenchmarkLogPrint-12                     2511703              1418 ns/op             262 B/op          6 allocs/op
BenchmarkLogLevelIgnore-12              310685697               11.3 ns/op             0 B/op          0 allocs/op
BenchmarkLoggerClone-12                  6193008               583 ns/op             480 B/op         10 allocs/op
BenchmarkLogPrintWithNoTime-12           4527244               795 ns/op             272 B/op          7 allocs/op
BenchmarkLogPrintJSON-12                 2060330              1754 ns/op             542 B/op          9 allocs/op
BenchmarkLogPrintWithStruct-12           1934262              1859 ns/op             558 B/op         10 allocs/op
BenchmarkLogPrintJSONWithStruct-12       1732045              2066 ns/op             581 B/op         12 allocs/op
```

### Commit:ae9a3ca86e6b75a338797f01d1fdfa5a15855879 Check at "Fri, 01 Jan 2021 21:58:28 +0800" 含"FX()"

```text
goos: windows
goarch: amd64
pkg: gitee.com/woodpile/utils.go/log
BenchmarkLogPrint-12                     2121273              1673 ns/op             294 B/op          9 allocs/op
BenchmarkLogLevelIgnore-12              248873002               14.4 ns/op             0 B/op          0 allocs/op
BenchmarkLoggerClone-12                  6218154               576 ns/op             480 B/op         10 allocs/op
BenchmarkLogPrintWithNoTime-12           3581370              1011 ns/op             304 B/op         10 allocs/op
BenchmarkLogPrintJSON-12                 1803873              2008 ns/op             574 B/op         12 allocs/op
BenchmarkLogPrintWithStruct-12           1691629              2118 ns/op             590 B/op         13 allocs/op
BenchmarkLogPrintJSONWithStruct-12       1550468              2349 ns/op             614 B/op         15 allocs/op
```

### Commit:5a46617efb48b07c3b81b070c6428903df9eb147 Check at "Thu Nov 26 23:31:07 2020 +0800"

```text
goos: windows
goarch: amd64
pkg: gitee.com/woodpile/utils.go/log
BenchmarkLogPrint-12                     1940335              1852 ns/op             395 B/op         14 allocs/op
BenchmarkLogLevelIgnore-12              249072946               14.4 ns/op             0 B/op          0 allocs/op
BenchmarkLoggerClone-12                  6194138               582 ns/op             480 B/op         10 allocs/op
BenchmarkLogPrintWithNoTime-12           3172622              1133 ns/op             368 B/op         14 allocs/op
BenchmarkLogPrintJSON-12                 1701787              2132 ns/op             672 B/op         17 allocs/op
BenchmarkLogPrintWithStruct-12           1635183              2213 ns/op             686 B/op         18 allocs/op
BenchmarkLogPrintJSONWithStruct-12       1495311              2416 ns/op             710 B/op         20 allocs/op
```

### Commit:0212759525e3fe8b4038610280fb5d6f7cd6e897 Check at "Tue, 07 May 2019 17:46:31 +0800"

```text
goos: windows
goarch: amd64
pkg: woodpile.org/utils.go/utils/log
BenchmarkLogPrint-4                      1000000              2083 ns/op             784 B/op         32 allocs/op
BenchmarkLogLevelIgnore-4               10000000               244 ns/op              72 B/op          6 allocs/op
BenchmarkLoggerClone-4                  10000000               286 ns/op             400 B/op          5 allocs/op
BenchmarkLogPrintWithNoTime-4            2000000              1303 ns/op             560 B/op         23 allocs/op
```

### Commit:2820ce4ea100cd36d49789b65c7ca5ca5c51060f Check at "Sun, 28 Apr 2019 17:36:18 +0800"

```text
goos: windows
goarch: amd64
BenchmarkLogPrint-4              2000000              2520 ns/op            1064 B/op         40 allocs/op
BenchmarkLogLevelIgnore-4       10000000               668 ns/op             352 B/op         14 allocs/op
BenchmarkLoggerClone-4          20000000               284 ns/op             368 B/op          5 allocs/op
```

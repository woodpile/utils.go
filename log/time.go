package log

import (
	"fmt"
	"strings"
	"time"
)

//FuncTimeFmt define declaration of time format function
type FuncTimeFmt func(*strings.Builder, time.Time)

//BuiltinTimeFmtStandard is builtin FuncTimeFmt with format "2019-04-22 16:03:45.223".
func BuiltinTimeFmtStandard(buff *strings.Builder, t time.Time) {
	year, month, day := t.Date()
	hour, minute, second := t.Clock()
	ms := t.Nanosecond() / 1000000
	fmt.Fprintf(buff, "%04d-%02d-%02d %02d:%02d:%02d.%03d",
		year, int(month), day, hour, minute, second, ms)
}

//BuiltinTimeFmtCompact is builtin FuncTimeFmt with format "20190422160345223".
func BuiltinTimeFmtCompact(buff *strings.Builder, t time.Time) {
	year, month, day := t.Date()
	hour, minute, second := t.Clock()
	ms := t.Nanosecond() / 1000000
	fmt.Fprintf(buff, "%04d%02d%02d%02d%02d%02d%03d",
		year, int(month), day, hour, minute, second, ms)
}

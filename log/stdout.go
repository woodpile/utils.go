package log

import (
	"fmt"
	"os"
)

//Stdout Target to stdout
var Stdout = &TargetStdout{}

//TargetStdout implement Target with stdout printing.
type TargetStdout struct {
}

//Name implement for Target.Name()
func (t *TargetStdout) Name() string {
	return "stdout"
}

//IsIgnore implement for Target.IsIgnore()
func (t *TargetStdout) IsIgnore(l int) bool {
	return false
}

//Put implement for Target.Put()
func (t *TargetStdout) Put(msg string) {
	fmt.Fprintln(os.Stdout, msg)
}

//Close implement for Target.Close()
func (t *TargetStdout) Close() {
	//standrad output doesn't need close
}

//IncrUsing implement for Target.IncrUsing()
func (t *TargetStdout) IncrUsing() {
	//standrad output doesn't need close
}

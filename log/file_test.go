package log

import (
	"os"
	"reflect"
	"testing"
)

var (
	gopathTmpDir string
)

func init() {
	gopathTmpDir = os.TempDir()
}

func TestMakeTargetFile(t *testing.T) {
	type args struct {
		dir string
		fn  string
	}
	tests := []struct {
		name    string
		args    args
		wantNil bool
		wantErr bool
	}{
		{
			name:    "ok.0",
			args:    args{dir: gopathTmpDir, fn: "tmp.log"},
			wantNil: false,
			wantErr: false,
		},
		{
			name:    "err.0",
			args:    args{dir: gopathTmpDir + "______", fn: "tmp.log"},
			wantNil: true,
			wantErr: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := MakeTargetFile(tt.args.dir, tt.args.fn)
			if (err != nil) != tt.wantErr {
				t.Errorf("MakeTargetFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if (nil == got) != tt.wantNil {
				t.Errorf("MakeTargetFile() = %v, wantNil %v", got, tt.wantNil)
			}
			if nil != got {
				got.Close()
			}
		})
	}
}

func TestTargetFile_Name(t *testing.T) {
	tests := []struct {
		name string
		t    *TargetFile
		want string
	}{
		{
			name: "0",
			t: func() *TargetFile {
				tf, _ := MakeTargetFile(gopathTmpDir, "aa.xx")
				return tf
			}(),
			want: "file.aa.xx",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.Name(); got != tt.want {
				t.Errorf("TargetFile.Name() = %v, want %v", got, tt.want)
			}
			tt.t.Close()
		})
	}
}

func TestTargetFile_IsIgnore(t *testing.T) {
	type args struct {
		l int
	}
	tests := []struct {
		name string
		t    *TargetFile
		args args
		want bool
	}{
		{
			name: "0",
			t: func() *TargetFile {
				tf, _ := MakeTargetFile(gopathTmpDir, "tmp.log")
				return tf
			}(),
			args: args{l: LvlDebug},
			want: false,
		},
		{
			name: "1",
			t: func() *TargetFile {
				tf, _ := MakeTargetFile(gopathTmpDir, "tmp.log")
				return tf
			}(),
			args: args{l: LvlError},
			want: false,
		},
		{
			name: "2",
			t: func() *TargetFile {
				tf, _ := MakeTargetFile(gopathTmpDir, "tmp.log")
				return tf
			}(),
			args: args{l: LvlInvalid},
			want: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.IsIgnore(tt.args.l); got != tt.want {
				t.Errorf("TargetFile.IsIgnore() = %v, want %v", got, tt.want)
			}
			tt.t.Close()
		})
	}
}

func TestTargetFile_SetLevels(t *testing.T) {
	type args struct {
		ls    []int
		check int
	}
	tests := []struct {
		name      string
		t         *TargetFile
		args      args
		wantCheck bool
	}{
		{
			name: "0",
			t: func() *TargetFile {
				tf, _ := MakeTargetFile(gopathTmpDir, "tmp.log")
				return tf
			}(),
			args:      args{ls: []int{LvlDebug}, check: LvlDebug},
			wantCheck: false,
		},
		{
			name: "1",
			t: func() *TargetFile {
				tf, _ := MakeTargetFile(gopathTmpDir, "tmp.log")
				return tf
			}(),
			args:      args{ls: []int{LvlDebug}, check: LvlInfo},
			wantCheck: true,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.t.SetLevels(tt.args.ls)
			if got := tt.t.IsIgnore(tt.args.check); got != tt.wantCheck {
				t.Errorf("TargetFile.SetLevels(). got = %v, wantCheck %v", got, tt.wantCheck)
			}
			tt.t.Close()
		})
	}
}

func TestTargetFile_Put(t *testing.T) {
	type args struct {
		msg string
	}
	tests := []struct {
		name string
		t    *TargetFile
		args args
	}{
		{
			name: "0",
			t: func() *TargetFile {
				tf, _ := MakeTargetFile(gopathTmpDir, "tmp.log")
				return tf
			}(),
			args: args{msg: "hello world"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.t.Put(tt.args.msg)
			tt.t.Close()
		})
	}
}

func TestTargetFile_ReopenFile(t *testing.T) {
	tests := []struct {
		name    string
		t       *TargetFile
		wantErr bool
	}{
		{
			name: "0",
			t: func() *TargetFile {
				tf, _ := MakeTargetFile(gopathTmpDir, "tmp.log")
				return tf
			}(),
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if err := tt.t.ReopenFile(); (err != nil) != tt.wantErr {
				t.Errorf("TargetFile.ReopenFile() error = %v, wantErr %v", err, tt.wantErr)
			}
			tt.t.Close()
		})
	}
}

func TestTargetFile_IncrUsing(t *testing.T) {
	tests := []struct {
		name string
		tr   *TargetFile
		want int32
	}{
		{
			name: "ok.0",
			tr:   &TargetFile{},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.tr.IncrUsing()
			if got := tt.tr.using; got != tt.want {
				t.Errorf("TargetFile.IncrUsing(). got = %v, want = %v", got, tt.want)
			}
		})
	}
}

func TestTargetFile_Close(t *testing.T) {
	tests := []struct {
		name string
		tr   *TargetFile
	}{
		//no need test
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.tr.Close()
		})
	}
}

func TestTargetFile_openFile(t *testing.T) {
	tests := []struct {
		name    string
		tr      *TargetFile
		want    *os.File
		wantErr bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := tt.tr.openFile()
			if (err != nil) != tt.wantErr {
				t.Errorf("TargetFile.openFile() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			defer got.Close()
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("TargetFile.openFile() = %v, want %v", got, tt.want)
			}
		})
	}
}

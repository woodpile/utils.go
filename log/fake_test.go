package log

import (
	"strings"
	"time"
)

//Target implement Target without printing.
//Output message will be collect as a queue.
//It is useful to check output string in unit test.
type FakeTarget struct {
	queue []string
	level int

	notcache bool
}

//Name implement for Target.Name().
func (t *FakeTarget) Name() string {
	return "fake"
}

//IncrUsing implement for Target.IncrUsing().
func (t *FakeTarget) IncrUsing() {

}

//IsIgnore implement for Target.IsIgnore().
func (t *FakeTarget) IsIgnore(l int) bool {
	return levelIgnore(t.level, l)
}

//Put implement for Target.Put().
func (t *FakeTarget) Put(msg string) {
	if t.notcache {
		return
	}
	t.queue = append(t.queue, msg)
}

//Close implement for Target.Close().
func (t *FakeTarget) Close() {

}

//Count return number of strings in queue.
func (t *FakeTarget) Count() int {
	return len(t.queue)
}

//Pop a string from queue and return it.
func (t *FakeTarget) Pop() string {
	if t.Count() == 0 {
		return ""
	}
	s := t.queue[0]
	t.queue = t.queue[1:]
	return s
}

func (t *FakeTarget) SetLevel(i int) {
	t.level = i
}

func (t *FakeTarget) SetNotCache() {
	t.notcache = true
}

func makeHandlerWithFakeTargetOnly(h *Handler) (*Handler, *FakeTarget) {
	rt := h
	if nil == rt {
		rt = NewHandlerWithConfig(GlobalHandlerConfig())
	}
	fake := &FakeTarget{}
	rt.common.Targets = []Target{fake}
	rt.BindTimeFmt(func(*strings.Builder, time.Time) {})
	return rt, fake
}

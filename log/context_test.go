package log

import (
	"reflect"
	"strings"
	"testing"
)

func _utGetEmptyContextBase(common *HandlerCommon, level int) *ContextBase {
	return &ContextBase{
		Common: common,

		level:        level,
		prefix:       make([]string, 0, 4),
		fmtKey:       make([]string, 0, 4),
		fmtVal:       make([]interface{}, 0, 4),
		fmtFieldKey:  make([]string, 0, 4),
		fmtFieldVal:  make([]FmtField, 0, 4),
		segFmt:       make([]string, 0, 4),
		segArgs:      make([][]interface{}, 0, 4),
		targets:      make([]Target, 0, 4),
		uniqueTarget: nil,

		buff:  nil,
		tmpTs: make([]Target, 0, 4),

		fAssemble: nil,
		fRelease:  nil,
	}
}

func TestNewContextBase(t *testing.T) {
	utCom01 := _utGetEmptyHandlerCommon()

	type args struct {
		c     *HandlerCommon
		level int
	}
	tests := []struct {
		name string
		args args
		want *ContextBase
	}{
		{
			name: "ok.0",
			args: args{
				c:     utCom01,
				level: LvlWarning,
			},
			want: _utGetEmptyContextBase(utCom01, LvlWarning),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := NewContextBase(tt.args.c, tt.args.level)
			got.buff = nil
			got.fAssemble = nil
			got.fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("NewContextBase() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_Clear(t *testing.T) {
	utCom01 := _utGetEmptyHandlerCommon()

	tests := []struct {
		name string
		ctx  *ContextBase
		want *ContextBase
	}{
		{
			name: "ok.0",
			ctx: &ContextBase{
				Common: utCom01,

				level:        LvlWarning,
				prefix:       make([]string, 0, 4),
				fmtKey:       make([]string, 0, 4),
				fmtVal:       make([]interface{}, 0, 4),
				fmtFieldKey:  make([]string, 0, 4),
				fmtFieldVal:  make([]FmtField, 0, 4),
				segFmt:       make([]string, 0, 4),
				segArgs:      make([][]interface{}, 0, 4),
				targets:      make([]Target, 0, 4),
				uniqueTarget: nil,

				buff:  new(strings.Builder),
				tmpTs: make([]Target, 0, 4),

				fAssemble: nil,
				fRelease:  nil,
			},
			want: _utGetEmptyContextBase(utCom01, GlobalLevel()),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.ctx.Pre("pre1").FS("s1", "str1").FU("u1", 123).P("ssss")

			tt.ctx.Clear()
			got := tt.ctx
			got.buff = nil
			got.fAssemble = nil
			got.fRelease = nil
			tt.want.buff = nil
			tt.want.fAssemble = nil
			tt.want.fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextBase.Clear(). got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_Level(t *testing.T) {
	tests := []struct {
		name string
		ctx  *ContextBase
		want int
	}{
		{
			name: "ok.0",
			ctx:  NewContextBase(utHandlerCommon01, LvlWarning),
			want: LvlWarning,
		},
		{
			name: "ok.1",
			ctx:  NewContextBase(utHandlerCommon01, LvlInfo),
			want: LvlInfo,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ctx.Level(); got != tt.want {
				t.Errorf("ContextBase.Level() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_SetLevel(t *testing.T) {
	type args struct {
		level int
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.ctx.SetLevel(tt.args.level)
		})
	}
}

func TestContextBase_Pre(t *testing.T) {
	type args struct {
		prefix string
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
		want Context
	}{
		{
			name: "ok.0",
			ctx: func() *ContextBase {
				ctx := NewContextBase(utHandlerCommon01, LvlDebug)
				return ctx
			}(),
			args: args{
				prefix: "pre1",
			},
			want: func() *ContextBase {
				ctx := NewContextBase(utHandlerCommon01, LvlDebug)
				ctx.prefix = []string{"pre1"}
				ctx.targetsTmp()
				return ctx
			}(),
		},
		{
			name: "ok.1",
			ctx: func() *ContextBase {
				ctx := NewContextBase(utHandlerCommon01, LvlDebug)
				ctx.Pre("pre1")
				return ctx
			}(),
			args: args{
				prefix: "pre2",
			},
			want: func() *ContextBase {
				ctx := NewContextBase(utHandlerCommon01, LvlDebug)
				ctx.prefix = []string{"pre1", "pre2"}
				ctx.targetsTmp()
				return ctx
			}(),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.ctx.Pre(tt.args.prefix)

			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			tt.want.(*ContextBase).buff = nil
			tt.want.(*ContextBase).fAssemble = nil
			tt.want.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextBase.Pre() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_FS(t *testing.T) {
	type args struct {
		key string
		val string
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
		want Context
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.ctx.FS(tt.args.key, tt.args.val)

			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			tt.want.(*ContextBase).buff = nil
			tt.want.(*ContextBase).fAssemble = nil
			tt.want.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextBase.FS() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_FI(t *testing.T) {
	type args struct {
		key string
		val int64
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
		want Context
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.ctx.FI(tt.args.key, tt.args.val)

			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			tt.want.(*ContextBase).buff = nil
			tt.want.(*ContextBase).fAssemble = nil
			tt.want.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextBase.FI() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_FU(t *testing.T) {
	type args struct {
		key string
		val uint64
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
		want Context
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.ctx.FU(tt.args.key, tt.args.val)

			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			tt.want.(*ContextBase).buff = nil
			tt.want.(*ContextBase).fAssemble = nil
			tt.want.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextBase.FU() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_FD(t *testing.T) {
	type args struct {
		key string
		val float64
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
		want Context
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.ctx.FD(tt.args.key, tt.args.val)

			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			tt.want.(*ContextBase).buff = nil
			tt.want.(*ContextBase).fAssemble = nil
			tt.want.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextBase.FD() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_FX(t *testing.T) {
	type args struct {
		key string
		val uint64
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
		want Context
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.ctx.FX(tt.args.key, tt.args.val)

			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			tt.want.(*ContextBase).buff = nil
			tt.want.(*ContextBase).fAssemble = nil
			tt.want.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextBase.FX() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_FT(t *testing.T) {
	type args struct {
		key string
		val FmtField
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
		want Context
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.ctx.FT(tt.args.key, tt.args.val)

			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			tt.want.(*ContextBase).buff = nil
			tt.want.(*ContextBase).fAssemble = nil
			tt.want.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextBase.FT() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_P(t *testing.T) {
	type args struct {
		msg string
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
		want Context
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.ctx.P(tt.args.msg)

			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			tt.want.(*ContextBase).buff = nil
			tt.want.(*ContextBase).fAssemble = nil
			tt.want.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextBase.P() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_PF(t *testing.T) {
	type args struct {
		msg  string
		args []interface{}
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
		want Context
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.ctx.PF(tt.args.msg, tt.args.args...)

			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			tt.want.(*ContextBase).buff = nil
			tt.want.(*ContextBase).fAssemble = nil
			tt.want.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextBase.PF() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_Tar(t *testing.T) {
	type args struct {
		t Target
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
		want Context
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.ctx.Tar(tt.args.t)

			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			tt.want.(*ContextBase).buff = nil
			tt.want.(*ContextBase).fAssemble = nil
			tt.want.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextBase.Tar() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_UniTar(t *testing.T) {
	type args struct {
		t Target
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
		want Context
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got := tt.ctx.UniTar(tt.args.t)

			got.(*ContextBase).buff = nil
			got.(*ContextBase).fAssemble = nil
			got.(*ContextBase).fRelease = nil
			tt.want.(*ContextBase).buff = nil
			tt.want.(*ContextBase).fAssemble = nil
			tt.want.(*ContextBase).fRelease = nil
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("ContextBase.UniTar() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_Z(t *testing.T) {
	tests := []struct {
		name string
		ctx  *ContextBase
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.ctx.Z()
		})
	}
}

func TestContextBase_targetsTmp(t *testing.T) {
	tests := []struct {
		name string
		ctx  *ContextBase
		want bool
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.ctx.targetsTmp(); got != tt.want {
				t.Errorf("ContextBase.targetsTmp() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestContextBase_getCallLine(t *testing.T) {
	tests := []struct {
		name string
		ctx  *ContextBase
		want string
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.ctx.assembleCallLine()
		})
	}
}

func TestContextBase_bindRelease(t *testing.T) {
	type args struct {
		f func(ctx Context)
	}
	tests := []struct {
		name string
		ctx  *ContextBase
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.ctx.bindRelease(tt.args.f)
		})
	}
}

func TestContextBase_release(t *testing.T) {
	tests := []struct {
		name string
		ctx  *ContextBase
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.ctx.release()
		})
	}
}

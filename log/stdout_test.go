package log

import (
	"testing"
)

func TestTargetStdout_Name(t *testing.T) {
	tests := []struct {
		name string
		t    *TargetStdout
		want string
	}{
		{
			name: "00",
			t:    Stdout,
			want: "stdout",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.Name(); got != tt.want {
				t.Errorf("TargetStdout.Name() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTargetStdout_IsIgnore(t *testing.T) {
	tests := []struct {
		name string
		t    *TargetStdout
		want bool
	}{
		{
			name: "00",
			t:    Stdout,
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := tt.t.IsIgnore(1); got != tt.want {
				t.Errorf("TargetStdout.IsIgnore() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestTargetStdout_Put(t *testing.T) {
	type args struct {
		msg string
	}
	tests := []struct {
		name string
		t    *TargetStdout
		args args
	}{
		// TODO: Add test cases.
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.t.Put(tt.args.msg)
		})
	}
}

func TestTargetStdout_Close(t *testing.T) {
	tests := []struct {
		name string
		tr   *TargetStdout
	}{
		//no need test
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.tr.Close()
		})
	}
}

func TestTargetStdout_IncrUsing(t *testing.T) {
	tests := []struct {
		name string
		tr   *TargetStdout
	}{
		//no need test
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.tr.IncrUsing()
		})
	}
}

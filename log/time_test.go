package log

import (
	"strings"
	"testing"
	"time"
)

func TestBuiltinTimeFmtStandard(t *testing.T) {
	type args struct {
		buff *strings.Builder
		t    time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{
			name: "00",
			args: args{
				buff: &strings.Builder{},
				t:    time.Date(2099, 7, 13, 8, 30, 0, 123000000, time.Local),
			},
			want: "2099-07-13 08:30:00.123",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			BuiltinTimeFmtStandard(tt.args.buff, tt.args.t)
			if got := tt.args.buff.String(); got != tt.want {
				t.Errorf("BuiltinTimeFmtStandard() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestBuiltinTimeFmtCompact(t *testing.T) {
	type args struct {
		buff *strings.Builder
		t    time.Time
	}
	tests := []struct {
		name string
		args args
		want string
	}{
		// TODO: Add test cases.
		{
			name: "00",
			args: args{
				buff: &strings.Builder{},
				t:    time.Date(2099, 7, 13, 8, 30, 0, 123000000, time.Local),
			},
			want: "20990713083000123",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			BuiltinTimeFmtCompact(tt.args.buff, tt.args.t)
			if got := tt.args.buff.String(); got != tt.want {
				t.Errorf("BuiltinTimeFmtCompact() = %v, want %v", got, tt.want)
			}
		})
	}
}

package log

//ContextNil this context doesnot do anything
type ContextNil struct {
}

//CtxNil this context doesnot do anything
var CtxNil = ContextAllowTar(&ContextNil{})

//Clear implement Context.Clear()
func (ctx *ContextNil) Clear() {
}

//Z implement Context.Z()
func (ctx *ContextNil) Z() {
}

//Level implement Context.Level()
func (ctx *ContextNil) Level() int {
	return LvlInvalid
}

//SetLevel implement Context.SetLevel()
func (ctx *ContextNil) SetLevel(l int) {
}

//Pre implement Context.Pre()
func (ctx *ContextNil) Pre(prefix string) Context {
	return ctx
}

//FS implement Context.FS()
func (ctx *ContextNil) FS(key string, val string) Context {
	return ctx
}

//FI implement Context.FI()
func (ctx *ContextNil) FI(key string, val int64) Context {
	return ctx
}

//FU implement Context.FU()
func (ctx *ContextNil) FU(key string, val uint64) Context {
	return ctx
}

//FD implement Context.FD()
func (ctx *ContextNil) FD(key string, val float64) Context {
	return ctx
}

//FX implement Context.FX()
func (ctx *ContextNil) FX(key string, val uint64) Context {
	return ctx
}

//FT implement Context.FT()
func (ctx *ContextNil) FT(key string, val FmtField) Context {
	return ctx
}

//P implement Context.P()
func (ctx *ContextNil) P(msg string) Context {
	return ctx
}

//PF implement Context.PF()
func (ctx *ContextNil) PF(format string, args ...interface{}) Context {
	return ctx
}

//Tar implement Context.Tar()
func (ctx *ContextNil) Tar(t Target) ContextAllowTar {
	return ctx
}

//UniTar implement Context.UniTar()
func (ctx *ContextNil) UniTar(t Target) Context {
	return ctx
}

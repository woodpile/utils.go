package log

import (
	"strings"
	"testing"
)

func TestFFArrInt64_AssembleAsLineSeperator(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFArrInt64
		args args
		want string
	}{
		{
			name: "ok.0",
			f:    []int64{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: ``,
		},
		{
			name: "ok.1",
			f:    []int64{37, -84},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: `37,-84`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsLineSeperator(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); got != tt.want {
				t.Errorf("FFArrInt64.AssembleAsLineSeperator(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFArrInt64_AssembleAsJSON(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFArrInt64
		args args
		want string
	}{
		{
			name: "ok.0",
			f:    []int64{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: `[]`,
		},
		{
			name: "ok.1",
			f:    []int64{37, -84},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: `[37,-84]`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsJSON(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); got != tt.want {
				t.Errorf("FFArrInt64.AssembleAsJSON(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFArrUint64_AssembleAsLineSeperator(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFArrUint64
		args args
		want string
	}{
		{
			name: "ok.0",
			f:    []uint64{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: ``,
		},
		{
			name: "ok.1",
			f:    []uint64{37, 84},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: `37,84`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsLineSeperator(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); got != tt.want {
				t.Errorf("FFArrUint64.AssembleAsLineSeperator(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFArrUint64_AssembleAsJSON(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFArrUint64
		args args
		want string
	}{
		{
			name: "ok.0",
			f:    []uint64{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: `[]`,
		},
		{
			name: "ok.1",
			f:    []uint64{37, 84},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: `[37,84]`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsJSON(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); got != tt.want {
				t.Errorf("FFArrUint64.AssembleAsJSON(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFArrFloat64_AssembleAsLineSeperator(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFArrFloat64
		args args
		want string
	}{
		{
			name: "ok.0",
			f:    []float64{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: ``,
		},
		{
			name: "ok.1",
			f:    []float64{37.21, 84.9},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: `37.21,84.9`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsLineSeperator(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); got != tt.want {
				t.Errorf("FFArrFloat64.AssembleAsLineSeperator(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFArrFloat64_AssembleAsJSON(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFArrFloat64
		args args
		want string
	}{
		{
			name: "ok.0",
			f:    []float64{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: `[]`,
		},
		{
			name: "ok.1",
			f:    []float64{37.21, 84.9},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: `[37.21,84.9]`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsJSON(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); got != tt.want {
				t.Errorf("FFArrFloat64.AssembleAsJSON(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFArrString_AssembleAsLineSeperator(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFArrString
		args args
		want string
	}{
		{
			name: "ok.0",
			f:    []string{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: ``,
		},
		{
			name: "ok.1",
			f:    []string{"aaa", "bbb"},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: `aaa,bbb`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsLineSeperator(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); got != tt.want {
				t.Errorf("FFArrString.AssembleAsLineSeperator(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFArrString_AssembleAsJSON(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFArrString
		args args
		want string
	}{
		{
			name: "ok.0",
			f:    []string{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: `[]`,
		},
		{
			name: "ok.1",
			f:    []string{"aaa", "bbb"},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: `["aaa","bbb"]`,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsJSON(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); got != tt.want {
				t.Errorf("FFArrString.AssembleAsJSON(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFMapStringInt64_AssembleAsLineSeperator(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFMapStringInt64
		args args
		want map[string]bool
	}{
		{
			name: "ok.0",
			f:    map[string]int64{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				``: true,
			},
		},
		{
			name: "ok.1",
			f:    map[string]int64{"aaa": 1, "bbb": -2},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				`aaa:1,bbb:-2`: true,
				`bbb:-2,aaa:1`: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsLineSeperator(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); true != tt.want[got] {
				t.Errorf("FFMapStringInt64.AssembleAsLineSeperator(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFMapStringInt64_AssembleAsJSON(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFMapStringInt64
		args args
		want map[string]bool
	}{
		{
			name: "ok.0",
			f:    map[string]int64{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				`{}`: true,
			},
		},
		{
			name: "ok.1",
			f:    map[string]int64{"aaa": 1, "bbb": -2},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				`{"aaa":1,"bbb":-2}`: true,
				`{"bbb":-2,"aaa":1}`: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsJSON(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); true != tt.want[got] {
				t.Errorf("FFMapStringInt64.AssembleAsJSON(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFMapStringUint64_AssembleAsLineSeperator(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFMapStringUint64
		args args
		want map[string]bool
	}{
		{
			name: "ok.0",
			f:    map[string]uint64{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				``: true,
			},
		},
		{
			name: "ok.1",
			f:    map[string]uint64{"aaa": 1, "bbb": 2},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				`aaa:1,bbb:2`: true,
				`bbb:2,aaa:1`: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsLineSeperator(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); true != tt.want[got] {
				t.Errorf("FFMapStringUint64.AssembleAsLineSeperator(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFMapStringUint64_AssembleAsJSON(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFMapStringUint64
		args args
		want map[string]bool
	}{
		{
			name: "ok.0",
			f:    map[string]uint64{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				`{}`: true,
			},
		},
		{
			name: "ok.1",
			f:    map[string]uint64{"aaa": 1, "bbb": 2},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				`{"aaa":1,"bbb":2}`: true,
				`{"bbb":2,"aaa":1}`: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsJSON(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); true != tt.want[got] {
				t.Errorf("FFMapStringUint64.AssembleAsJSON(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFMapStringFloat64_AssembleAsLineSeperator(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFMapStringFloat64
		args args
		want map[string]bool
	}{
		{
			name: "ok.0",
			f:    map[string]float64{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				``: true,
			},
		},
		{
			name: "ok.1",
			f:    map[string]float64{"aaa": 1.1, "bbb": 2.678},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				`aaa:1.1,bbb:2.678`: true,
				`bbb:2.678,aaa:1.1`: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsLineSeperator(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); true != tt.want[got] {
				t.Errorf("FFMapStringFloat64.AssembleAsLineSeperator(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFMapStringFloat64_AssembleAsJSON(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFMapStringFloat64
		args args
		want map[string]bool
	}{
		{
			name: "ok.0",
			f:    map[string]float64{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				`{}`: true,
			},
		},
		{
			name: "ok.1",
			f:    map[string]float64{"aaa": 1.1, "bbb": 2.678},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				`{"aaa":1.1,"bbb":2.678}`: true,
				`{"bbb":2.678,"aaa":1.1}`: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsJSON(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); true != tt.want[got] {
				t.Errorf("FFMapStringFloat64.AssembleAsJSON(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFMapStringString_AssembleAsLineSeperator(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFMapStringString
		args args
		want map[string]bool
	}{
		{
			name: "ok.0",
			f:    map[string]string{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				``: true,
			},
		},
		{
			name: "ok.1",
			f:    map[string]string{"aaa": "1.1", "bbb": "ccc"},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				`aaa:1.1,bbb:ccc`: true,
				`bbb:ccc,aaa:1.1`: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsLineSeperator(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); true != tt.want[got] {
				t.Errorf("FFMapStringString.AssembleAsLineSeperator(), got %v, want %v", got, tt.want)
			}
		})
	}
}

func TestFFMapStringString_AssembleAsJSON(t *testing.T) {
	type args struct {
		buff *strings.Builder
		com  *HandlerCommon
	}
	tests := []struct {
		name string
		f    FFMapStringString
		args args
		want map[string]bool
	}{
		{
			name: "ok.0",
			f:    map[string]string{},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				`{}`: true,
			},
		},
		{
			name: "ok.1",
			f:    map[string]string{"aaa": "1.1", "bbb": "ccc"},
			args: args{
				buff: new(strings.Builder),
				com:  _utGetEmptyHandlerCommon(),
			},
			want: map[string]bool{
				`{"aaa":"1.1","bbb":"ccc"}`: true,
				`{"bbb":"ccc","aaa":"1.1"}`: true,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.f.AssembleAsJSON(tt.args.buff, tt.args.com)
			if got := tt.args.buff.String(); true != tt.want[got] {
				t.Errorf("FFMapStringString.AssembleAsJSON(), got %v, want %v", got, tt.want)
			}
		})
	}
}
